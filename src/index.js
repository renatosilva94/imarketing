import TelaLogin from './TelaLogin';
import TelaCadastro from './TelaCadastro';
import PaginaInicial from './PaginaInicial';
import PaginaQuiz from './PaginaQuiz';
import { createAppContainer, createStackNavigator, createBottomTabNavigator, createDrawerNavigator } from 'react-navigation';


const App  = createAppContainer(
    createDrawerNavigator({
    Login: TelaLogin,
    Cadastro: TelaCadastro,
    Inicio: PaginaInicial,
    Quiz: PaginaQuiz
  })
);



// const TabNavigator = createAppContainer(
//   createBottomTabNavigator({
//     Login: TelaLogin,
//   })
// );

const DrawerNavigator = createAppContainer(
  createDrawerNavigator({
    Login: TelaLogin,
    Cadastro: TelaCadastro,
    Inicio: PaginaInicial,
    Quiz: PaginaQuiz
  })
);

export default App;