import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TextInput, Button, TouchableOpacity } from 'react-native';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { ScrollView } from 'react-native-gesture-handler';

import Wizard from './components/Wizard'
import Input from './components/Input';

// const forms = [
//     {
//         placeholder: 'Username here...',
//         name: 'username',
//     },
//     {
//         placeholder: 'Email here...',
//         name: 'email',
//     },
//     {
//         placeholder: 'Avatar here...',
//         name: 'avatar',
//     },
// ];


export default class PaginaQuiz extends Component {

    render() {
        return (
            <View style={styles.root}>
                <Wizard>



                    <Wizard.Step>
                        <View style={styles.container}>




                            <Text style={styles.texto}>Como é o envolvimento de sua empresa com a comunidade ao redor?</Text>

                            <TouchableOpacity>
                                <Text style={styles.loginButton}>
                                    Não costumamos nos relacionar com empresas/pessoas que não façam parte da organização.
                                        </Text>
                            </TouchableOpacity >

                            <TouchableOpacity>
                                <Text style={styles.loginButton}>
                                    Nos relacionamos com pessoas de fora da organização esporadicamente.
                                        </Text>
                            </TouchableOpacity >

                            <TouchableOpacity>
                                <Text style={styles.loginButton}>
                                    Fazemos ações de cunho social frequentemente (Essa alternativa é cabível e condizente com o restante?)
                                        </Text>
                            </TouchableOpacity >

                        </View>

                    </Wizard.Step>



                    <Wizard.Step>
                        <View style={styles.container}>
                            <Text style={styles.texto}>
                                Como você avalia a vantagem competitiva de sua empresa em comparação a suas concorrentes?     </Text>


                            <TouchableOpacity>
                                <Text style={styles.loginButton}>
                                    Temos práticas/produtos inovadores em comparação com nossos concorrentes.
                                        </Text>
                            </TouchableOpacity >

                            <TouchableOpacity>
                                <Text style={styles.loginButton}>
                                    Nossos produtos e práticas são similares aos nossos concorrentes.
                                        </Text>
                            </TouchableOpacity >

                            <TouchableOpacity>
                                <Text style={styles.loginButton}>
                                    Nossos concorrentes possuem vantagens competitivas que ainda não conseguimos igualar.
                                        </Text>
                            </TouchableOpacity >



                        </View>


                    </Wizard.Step>







                    <Wizard.Step>
                        <View style={styles.container}>
                            <Text style={styles.texto}>
                            Qual das seguintes afirmações melhor condiz com o comportamento da sua empresa?     </Text>


                                <TouchableOpacity>
                                <Text style={styles.loginButton}>
                                O cliente da nossa empresa tem sempre razão!                                        </Text>
                            </TouchableOpacity >
                            
                            <TouchableOpacity>
                                <Text style={styles.loginButton}>
                                Às vezes achamos que nós temos a razão, e tentamos explicar para o nosso cliente o porquê.                                        </Text>
                            </TouchableOpacity >
                            
                            <TouchableOpacity>
                                <Text style={styles.loginButton}>
                                Nossa empresa tem o conhecimento, e o cliente precisa aceitar isto.

                                        </Text>
                            </TouchableOpacity >



                        </View>


                    </Wizard.Step>



                </Wizard>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    // container:{
    //     flex: 1, 
    //       backgroundColor: '#fff',
    //       alignItems:  'center',
    //     justifyContent: 'center'
    //   },


    quizContainer: {
        alignItems: 'center',
        flexGrow: 0.2,
        justifyContent: 'center',
    },

    texto: {
        fontSize: 30,
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: 'Roboto',
        color: '#222222'
    },

    loginButton: {
        alignItems: 'center',
        backgroundColor: '#000',
        margin: 10,
        color: '#fff',
        textAlign: 'center',
        fontSize: 20,
        padding: 10
    },

});