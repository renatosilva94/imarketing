import React, { PureComponent } from 'react';
import { View, Button, StyleSheet, Text } from 'react-native';

class Step extends PureComponent {
  state = {};
  render() {
    return (
      <View style={styles.root}>
        {/* {this.props.children({
          onChangeValue: this.props.onChangeValue,
          values: this.props.values,
        })} */}

        {this.props.children}

        <View style={styles.buttonWrapper}>
          <Button color="#000"
            title="Voltar"
            disabled={this.props.currentIndex === 0}
            onPress={this.props.prevStep}
          />
          {this.props.isLast ? (
            <Button color="#000" title="Salvar" onPress={this.props.onSubmit} />
          ) : (
            <Button color="#000" title="Próxima" onPress={this.props.nextStep} />
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  buttonWrapper: {
    flexDirection: 'row',
    height: 80,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
});

export default Step;
