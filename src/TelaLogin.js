import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TextInput, Button, TouchableOpacity, Keyboard, Alert } from 'react-native';
import { SocialIcon } from 'react-native-elements';
// import firebase from 'react-native-firebase';
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';

export default class TelaLogin extends Component {

 

  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null,
    };
  }

  paginaInicial() {
    this.props.navigation.navigate('Inicio');
  }

  componentWillMount () {
    console.log(AccessToken)
    AccessToken.getCurrentAccessToken().then(
      (data) => {
        if(data)
        this.paginaInicial();
      }
    )
  }


  loginFacebook = async () => { 

    try {
      LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
        (result) => {
          AccessToken.getCurrentAccessToken().then((data) => {
            if (result.isCancelled) {
              alert('Login Cancelado');
            }
            else {
              //alert('Login Com Sucesso' + data.accessToken.toString());
              //this.paginaInicialPosLogin();
              this.paginaInicial();

            }
          },
            function (error) {
              alert('Login Falhou: ' + error);
            }
          )
        }
      );
    } catch (error) {
      alert('Erro Indefinido')
    }

    
  }


  

  


logUsuario = () => {
  fetch('http://192.168.0.10/imarketing-backend/public/api/loginUsuario', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "email": this.state.email,
      "password": this.state.password,

    }),
  })
    .then((response) => response.json())
    .then((responseData) => {
      "POST Response",
        Alert.alert(JSON.stringify(responseData))
    })
    .done();
  // this.input._root.clear();
  Keyboard.dismiss();
};

handleChange = e => {
  this.setState({
    email: e.nativeEvent.text,
    password: e.nativeEvent.text,

  });
};


render() {
  return (
    <View style={styles.container}>
      <View style={styles.loginContainer}>

        <Image resizeMode="contain"
          style={{
            width: 180, height: 180, marginBottom: 40
          }}
          source={{ uri: '@drawable/icon' }}
        />

        <Text style={styles.texto}>Bem-Vindo(a) ao <Text style={{ fontWeight: 'bold' }}>iMarketing</Text></Text>

      </View>

      <View style={styles.loginContainer}>
        <TextInput
          placeholder={'E-MAIL'}
          style={styles.input}
          // onChange={this.handleChange}
          onChangeText={input => this.setState({ email: input })} ref={(ref) => { this.input = ref }}
          name={'email'}

        />


        <TextInput
          placeholder={'SENHA'}
          secureTextEntry={true}
          style={styles.input}
          // onChange={this.handleChange}
          onChangeText={input => this.setState({ password: input })} ref={(ref) => { this.input = ref }}
          name={'password'}

        />

        <TouchableOpacity onPress={() => this.logUsuario()}>
          <Text style={styles.loginButton}>
            ENTRAR
              </Text>
        </TouchableOpacity>

        {/* {this.state.isAuthenticated ? <Text>Efetuando Login ...</Text> : null} */}


      </View>


      <View>

        <Text style={{ fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>
          LOGIN COM AS REDES SOCIAIS
              </Text>

        <View style={styles.iconesSociais}>





          <SocialIcon
            type='facebook'
            onPress={this.loginFacebook}
          />

          <SocialIcon
            type='google-plus-official'
            onPress={() => {
              //Action to perform on press of Social Icon
              alert('google');
            }}
          />



        </View>
      </View>


    </View>
  );






}
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  loginContainer: {
    alignItems: 'center',
    flexGrow: 0.2,
    justifyContent: 'center'
  },
  input: {
    alignItems: 'center',
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderRadius: 15,
    borderColor: 'black',
    marginBottom: 10,
    textAlign: 'center'
  },
  loginButton: {
    alignItems: 'center',
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderRadius: 15,
    borderColor: '#fff',
    backgroundColor: '#000',
    marginBottom: 10,
    color: '#fff',
    textAlign: 'center'

  },



  iconesSociais: {
    alignItems: 'center',
    marginBottom: 10,
    textAlign: 'center',
    justifyContent: 'center',
    flexDirection: 'row',

  },

  texto: {
    fontSize: 20
  }



  // container: {
  //   flex: 1,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   backgroundColor: '#F5FCFF',
  // },
  // welcome: {
  //   fontSize: 20,
  //   textAlign: 'center',
  //   margin: 10,
  // },
  // instructions: {
  //   textAlign: 'center',
  //   color: '#333333',
  //   marginBottom: 5,
  // },



});
