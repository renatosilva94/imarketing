import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { LoginManager, LoginButton, AccessToken, GraphRequestManager, GraphRequest } from 'react-native-fbsdk';



export default class PaginaInicial extends Component {

  

//Create response callback.
_responseInfoCallback = (error, result) => {
    if (error) {
      alert('Error fetching data: ' + error.toString());
    } else {
      this.setState({name: result.name, picture: result.picture.data.url, email: result.email});
    }
  }

  componentWillMount() {

    const infoRequest = new GraphRequest(
      '/me?fields=name,picture.type(large),email',
      null,
      this._responseInfoCallback
    );

    new GraphRequestManager().addRequest(infoRequest).start();
  }

  constructor() {
    super();
    this.state = {
      name : '',
      picture : '',
      email: ''
    }
  }
    
     
render() {
    return (
      <View style={styles.container}>
          <View style={styles.header}></View>
          <Image style={styles.avatar} source={{uri:this.state.picture}}/>
          <View style={styles.body}>
            <View style={styles.bodyContent}>
              <Text style={styles.name}>{this.state.name}</Text>
              <Text style={styles.info}>{this.state.email}</Text>
              <Text style={styles.description}>Lorem ipsum dolor sit amet, saepe sapientem eu nam. Qui ne assum electram expetendis, omittam deseruisse consequuntur ius an,</Text>
              
              <TouchableOpacity style={styles.buttonContainer} >
                <Text>Editar Perfil</Text>  
              </TouchableOpacity>   

              <TouchableOpacity style={styles.buttonContainer} >
                <Text>Assessoria de Marketing</Text>  
              </TouchableOpacity> 

              <TouchableOpacity style={styles.buttonContainer} onPress={LoginManager.logOut()}>
                <Text>Sair</Text> 
              </TouchableOpacity>
            </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    backgroundColor: "#000",
    height:200,
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom:10,
    alignSelf:'center',
    position: 'absolute',
    marginTop:130
  },
  name:{
    fontSize:22,
    color:"#FFFFFF",
    fontWeight:'600',
  },
  body:{
    marginTop:40,
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    padding:30,
  },
  name:{
    fontSize:28,
    color: "#696969",
    fontWeight: "600"
  },
  info:{
    fontSize:16,
    color: "#000",
    marginTop:10
  },
  description:{
    fontSize:16,
    color: "#696969",
    marginTop:10,
    textAlign: 'center'
  },
  buttonContainer: {
    marginTop:10,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
    backgroundColor: "#fff",
    borderColor: '#000',
    borderWidth: 2
  },
});