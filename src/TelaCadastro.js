import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Keyboard, Alert } from 'react-native';
import { SocialIcon } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
// import {db} from '../src/config'
import AsyncStorage from '@react-native-community/async-storage';
import { Button, Card } from "native-base";

// const TelaCadastro = ({ navigation }) => (

// let cadUsuario = usuario => {  
//   db.ref('/usuarios').push({
//     usuario: usuario
//   });
// };



export default class TelaCadastro extends Component {

  constructor(props) {
    super(props);
    this.state = {
      nome: null,
      sobrenome: null,
      email: null,
      password: null,
      nome_empresa: null,
      area_atuacao_empresa: null,
      tamanho_empresa: null,
      funcao: null,
    };
  }

  cadUsuario = () => {
    fetch('http://192.168.0.10/imarketing-backend/public/api/cadastrarUsuario', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "nome": this.state.nome,
        "sobrenome": this.state.sobrenome,
        "email": this.state.email,
        "password": this.state.password,
        "nome_empresa": this.state.nome_empresa,
        "area_atuacao_empresa": this.state.area_atuacao_empresa,
        "tamanho_empresa": this.state.tamanho_empresa,
        "funcao": this.state.funcao,

      }),
    })
      .then((response) => response.json())
      .then((responseData) => {
        "POST Response",
        Alert.alert(JSON.stringify(responseData))
      })
      .done();
    // this.input._root.clear();
    Keyboard.dismiss();
  };



  handleChange = e => {
    this.setState({
      nome: e.nativeEvent.text,
      sobrenome: e.nativeEvent.text,
      email: e.nativeEvent.text,
      password: e.nativeEvent.text,
      nome_empresa: e.nativeEvent.text,
      area_atuacao_empresa: e.nativeEvent.text,
      tamanho_empresa: e.nativeEvent.text,
      funcao: e.nativeEvent.text,

      // [e.target.name]: e.target.value

    });
  };
  // handleSubmit = () => {
  //   cadUsuario(this.state);
  //   alert('Usuário Cadastrado Com Sucesso');
  // };

  render() {
    return (

      <ScrollView style={styles.container}>
        <View style={styles.loginContainer}>

          <Image resizeMode="contain"
            style={{
              width: 180, height: 180, marginBottom: 40
            }}
            source={{ uri: '@drawable/icon' }}
          />

          <Text style={styles.texto}>Cadastro de Usuário</Text>

        </View>

        <View style={styles.loginContainer}>

          <TextInput
            placeholder={'NOME'}
            style={styles.input}
            // onChange={this.handleChange}
            onChangeText={input => this.setState({ nome: input })} ref={(ref) => { this.input = ref }}
            name={'nome'}
          />

          <TextInput
            placeholder={'SOBRENOME'}
            style={styles.input}
            // onChange={this.handleChange}
            onChangeText={input => this.setState({ sobrenome: input })} ref={(ref) => { this.input = ref }}
            name={'sobrenome'}
          />

          <TextInput
            placeholder={'E-MAIL'}
            style={styles.input}
            // onChange={this.handleChange}
            onChangeText={input => this.setState({ email: input })} ref={(ref) => { this.input = ref }}
            name={'email'}
          />

          <TextInput
            placeholder={'SENHA'}
            style={styles.input}
            secureTextEntry={true}
            // onChange={this.handleChange}
            onChangeText={input => this.setState({ password: input })} ref={(ref) => { this.input = ref }}
            name={'password'}
          />

          <TextInput
            placeholder={'EMPRESA EM QUE TRABALHA'}
            style={styles.input}
            // onChange={this.handleChange}
            onChangeText={input => this.setState({ nome_empresa: input })} ref={(ref) => { this.input = ref }}
            name={'nome_empresa'}

          />

          <TextInput
            placeholder={'ÁREA DE ATUAÇÃO DA EMPRESA'}
            style={styles.input}
            // onChange={this.handleChange}
            onChangeText={input => this.setState({ area_atuacao_empresa: input })} ref={(ref) => { this.input = ref }}
            name={'area_atuacao_empresa'}


          />

          <TextInput
            placeholder={'TAMANHO DA EMPRESA'}
            style={styles.input}
            // onChange={this.handleChange}
            onChangeText={input => this.setState({ tamanho_empresa: input })} ref={(ref) => { this.input = ref }}
            name={'tamanho_empresa'}


          />

          <TextInput
            placeholder={'SUA FUNÇÃO'}
            style={styles.input}
            // onChange={this.handleChange}
            onChangeText={input => this.setState({ funcao: input })} ref={(ref) => { this.input = ref }}
            name={'funcao'}


          />

          <TouchableOpacity onPress={ () => this.cadUsuario() }>
            <Text style={styles.loginButton}>
              CADASTRAR
            </Text>
          </TouchableOpacity >


        </View>





      </ScrollView>

    );

  }
}


const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  loginContainer: {
    alignItems: 'center',
    flexGrow: 0.2,
    justifyContent: 'center'
  },
  input: {
    alignItems: 'center',
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderRadius: 15,
    borderColor: 'black',
    marginBottom: 10,
    textAlign: 'center'
  },

  loginButton: {
    alignItems: 'center',
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderRadius: 15,
    borderColor: '#fff',
    backgroundColor: '#000',
    marginBottom: 10,
    color: '#fff',
    textAlign: 'center'

  },

  texto: {
    fontSize: 20
  }



  // container: {
  //   flex: 1,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   backgroundColor: '#F5FCFF',
  // },
  // welcome: {
  //   fontSize: 20,
  //   textAlign: 'center',
  //   margin: 10,
  // },
  // instructions: {
  //   textAlign: 'center',
  //   color: '#333333',
  //   marginBottom: 5,
  // },



});
